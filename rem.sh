#!/bin/bash


ST_FOLDER_LOC="/home/${SUDO_USER:-$USER}/Desktop/st/"
trash-put -v '/usr/local/bin/st'
trash-put -v '/usr/local/bin/st-copyout'
trash-put -v '/usr/local/bin/st-urlhandler'
trash-put -v '/usr/share/terminfo/s/st'*
trash-put -v '/usr/share/man/man4/st.4.gz'
trash-put -v "${ST_FOLDER_LOC}"*.o
trash-put -v "${ST_FOLDER_LOC}"st

